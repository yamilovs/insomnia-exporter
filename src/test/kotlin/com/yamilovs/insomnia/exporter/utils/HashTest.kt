package com.yamilovs.insomnia.exporter.utils

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe

internal class HashTest : StringSpec() {
    init {
        "Check that SHA-1 digest works properly" {
            Hash.sha1("Hello World") shouldBe "0a4d55a8d778e5022fab701977c5d840bbc486d0"
        }

        "Check that random SHA-1 digest will never match" {
            val howMany = 1000
            generateSequence { Hash.generateRandomSha1() }.take(howMany).toSet().size shouldBe howMany
        }
    }
}