package com.yamilovs.insomnia.exporter.utils

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe

internal class PropertyOrderTest : StringSpec() {
    init {
        "Check that empty data will return nullable value" {
            PropertyOrder.compute(mapOf()) shouldBe null
        }

        "Check computing one level property order" {
            val input = mapOf("FOO" to "1", "BAR" to "2", "BAZ" to "3")
            val expected = mapOf("&" to listOf("FOO", "BAR", "BAZ"))

            PropertyOrder.compute(input) shouldBe expected
        }

        "Check computing multilevel properties" {
            val input: Map<String, Any> = mapOf(
                "LEVEL_1_1" to "1_1", // simple value
                "LEVEL_1_2" to listOf(1, 2, 3), // collection value
                "LEVEL_1_3" to mapOf( // object value
                    "LEVEL_1_3_1" to "1_3_1",
                    "LEVEL_1_3_2" to mapOf(
                        "LEVEL_1_3_2_1" to 1,
                        "LEVEL_1_3_2_2" to 2,
                    ),
                    "LEVEL_1_3_3" to listOf(1, 2, 3),
                    "LEVEL_1_3_4" to 134
                ),
            )
            val expected = mapOf(
                "&" to listOf("LEVEL_1_1", "LEVEL_1_2", "LEVEL_1_3"),
                "&~|LEVEL_1_3" to listOf("LEVEL_1_3_1", "LEVEL_1_3_2", "LEVEL_1_3_3", "LEVEL_1_3_4"),
                "&~|LEVEL_1_3~|LEVEL_1_3_2" to listOf("LEVEL_1_3_2_1", "LEVEL_1_3_2_2"),
            )

            PropertyOrder.compute(input) shouldBe expected
        }
    }
}