package com.yamilovs.insomnia.exporter.dsl

import com.yamilovs.insomnia.exporter.model.Request
import com.yamilovs.insomnia.exporter.model.RequestGroup
import com.yamilovs.insomnia.exporter.model.Workspace
import com.yamilovs.insomnia.exporter.model.request.FollowRedirect.*
import com.yamilovs.insomnia.exporter.model.request.Method.*
import com.yamilovs.insomnia.exporter.model.request.authentication.EmptyAuthentication
import com.yamilovs.insomnia.exporter.model.request.body.EmptyBody
import com.yamilovs.insomnia.exporter.model.request.header.HeaderBase
import com.yamilovs.insomnia.exporter.model.request.header.IHeader
import com.yamilovs.insomnia.exporter.model.request.parameter.IParameter
import com.yamilovs.insomnia.exporter.model.request.parameter.ParameterBase
import io.kotest.assertions.throwables.shouldThrowExactly
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.shouldContain
import java.net.URL

class RequestBuilderTest : StringSpec() {
    private val requestChecker = object : ResourceChecker<Request, RequestBuilder>(
        builderInit = { RequestBuilder(Workspace()).apply { name(""); url("") } }
    ) {}

    init {
        "Check parents with DSL building" {
            val workspace = Workspace()
            val group = RequestGroup(parent = workspace, name = "Group")
            val baseInit: RequestBuilder.() -> Unit = {
                name { "Request name" }
                url { "https://yamilovs.com" }
            }

            val workspaceRequest = Request.build(workspace) { baseInit() }
            val groupRequest = Request.build(group) { baseInit() }

            workspaceRequest.parentId shouldBe workspace.id
            groupRequest.parentId shouldBe group.id
        }

        "Check default DSL building" {
            val request = Request.build(Workspace()) {
                name { "Request name" }
                url { "https://yamilovs.com" }
            }

            request.name shouldBe "Request name"
            request.url shouldBe "https://yamilovs.com"
        }

        "Check that name is required for builder" {
            val e = shouldThrowExactly<IllegalArgumentException> {
                Request.build(Workspace()) { url { "https://yamilovs.com" } }
            }
            e.message shouldContain "name"
        }

        "Check that url is required for builder" {
            val e = shouldThrowExactly<IllegalArgumentException> {
                Request.build(Workspace()) { name { "Request name" } }
            }
            e.message shouldContain "url"
        }

        "Check url DSL methods" {
            with(requestChecker) {
                checkDSLMethod("https://yamilovs.com", { url }, { url("https://yamilovs.com") })
                checkDSLMethod("https://yamilovs.com", { url }, { url { "https://yamilovs.com" } })
                checkDSLMethod("https://yamilovs.com", { url }, { +URL("https://yamilovs.com") })
                checkDSLMethod("https://yamilovs.com:443/vc", { url }, { +URL("https", "yamilovs.com", 443, "/vc") })
            }
        }

        "Check description DSL methods" {
            with(requestChecker) {
                checkDSLMethod("description", { description }, { description("description") })
                checkDSLMethod("description", { description }, { description { "description" } })
            }
        }

        "Check method DSL methods" {
            with(requestChecker) {
                checkDSLMethod(GET, { method }, {})
                checkDSLMethod(POST, { method }, { +POST })
                checkDSLMethod(PUT, { method }, { method(PUT) })
                checkDSLMethod(DELETE, { method }, { method { DELETE } })
            }
        }

        "Check body DSL methods" {
            with(requestChecker) {
                checkDSLMethod(EmptyBody, { body }, {})
                checkDSLMethod(EmptyBody, { body }, { body(EmptyBody) })
                checkDSLMethod(
                    EmptyBody,
                    { body },
                    { body { EmptyBody } })
                checkDSLMethod(
                    EmptyBody,
                    { body },
                    { +EmptyBody })
            }
        }

        "Check parameters DSL methods" {
            val param = object : ParameterBase() {
                override val name = ""
                override val value = ""
                override val description = ""
                override val disabled = false
            }

            with(requestChecker) {
                checkDSLMethod(listOf<IParameter>(), { parameters }, {})
                checkDSLMethod(listOf(param), { parameters }, { parameter(param) })
                checkDSLMethod(listOf(param), { parameters }, { parameter { param } })
                checkDSLMethod(listOf(param), { parameters }, { +param })
                checkDSLMethod(listOf(param), { parameters }, { parameters(listOf(param)) })
                checkDSLMethod(listOf(param), { parameters }, { parameters { listOf(param) } })
            }
        }

        "Check headers DSL methods" {
            val header = object : HeaderBase() {
                override val name = ""
                override val value = ""
                override val description = ""
                override val disabled = false
            }

            with(requestChecker) {
                checkDSLMethod(listOf<IHeader>(), { headers }, {})
                checkDSLMethod(listOf(header), { headers }, { header(header) })
                checkDSLMethod(listOf(header), { headers }, { header { header } })
                checkDSLMethod(listOf(header), { headers }, { +header })
                checkDSLMethod(listOf(header), { headers }, { headers(listOf(header)) })
                checkDSLMethod(listOf(header), { headers }, { headers { listOf(header) } })
            }
        }

        "Check authentication DSL methods" {
            with(requestChecker) {
                checkDSLMethod(EmptyAuthentication, { authentication }, {})
                checkDSLMethod(EmptyAuthentication, { authentication }, { +EmptyAuthentication })
                checkDSLMethod(EmptyAuthentication, { authentication }, { authentication(EmptyAuthentication) })
                checkDSLMethod(EmptyAuthentication, { authentication }, { authentication { EmptyAuthentication } })
            }
        }

        "Check metaSortKey DSL methods" {
            with(requestChecker) {
                checkDSLMethod(0, { metaSortKey }, {})
                checkDSLMethod(100, { metaSortKey }, { metaSortKey(100) })
                checkDSLMethod(100, { metaSortKey }, { metaSortKey { 100 } })
            }
        }

        "Check private DSL methods" {
            with(requestChecker) {
                checkDSLMethod(false, { isPrivate }, {})
                checkDSLMethod(true, { isPrivate }, { private(true) })
                checkDSLMethod(true, { isPrivate }, { private { true } })
            }
        }

        "Check storeCookies DSL methods" {
            with(requestChecker) {
                checkDSLMethod(true, { settingStoreCookies }, {})
                checkDSLMethod(false, { settingStoreCookies }, { storeCookies(false) })
                checkDSLMethod(false, { settingStoreCookies }, { storeCookies { false } })
            }
        }

        "Check sendCookies DSL methods" {
            with(requestChecker) {
                checkDSLMethod(true, { settingSendCookies }, {})
                checkDSLMethod(false, { settingSendCookies }, { sendCookies(false) })
                checkDSLMethod(false, { settingSendCookies }, { sendCookies { false } })
            }
        }

        "Check disableRenderRequestBody DSL methods" {
            with(requestChecker) {
                checkDSLMethod(false, { settingDisableRenderRequestBody }, {})
                checkDSLMethod(true, { settingDisableRenderRequestBody }, { disableRenderRequestBody(true) })
                checkDSLMethod(true, { settingDisableRenderRequestBody }, { disableRenderRequestBody { true } })
            }
        }

        "Check encodeUrl DSL methods" {
            with(requestChecker) {
                checkDSLMethod(true, { settingEncodeUrl }, {})
                checkDSLMethod(false, { settingEncodeUrl }, { encodeUrl(false) })
                checkDSLMethod(false, { settingEncodeUrl }, { encodeUrl { false } })
            }
        }

        "Check rebuildPath DSL methods" {
            with(requestChecker) {
                checkDSLMethod(true, { settingRebuildPath }, {})
                checkDSLMethod(false, { settingRebuildPath }, { rebuildPath(false) })
                checkDSLMethod(false, { settingRebuildPath }, { rebuildPath { false } })
            }
        }

        "Check followRedirects DSL methods" {
            with(requestChecker) {
                checkDSLMethod(GLOBAL, { settingFollowRedirects }, {})
                checkDSLMethod(ON, { settingFollowRedirects }, { followRedirects(ON) })
                checkDSLMethod(OFF, { settingFollowRedirects }, { followRedirects { OFF } })
                checkDSLMethod(ON, { settingFollowRedirects }, { +ON })
            }
        }
    }
}
