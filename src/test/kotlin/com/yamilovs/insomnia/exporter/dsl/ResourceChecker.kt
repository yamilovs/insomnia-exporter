package com.yamilovs.insomnia.exporter.dsl

import com.yamilovs.insomnia.exporter.model.IResource
import io.kotest.matchers.shouldBe

internal abstract class ResourceChecker<RESOURCE : IResource, BUILDER : IResourceBuilder<RESOURCE>>(
    private val builderInit: () -> BUILDER
) {
    fun checkDSLMethod(
        expectedValue: Any?,
        getter: RESOURCE.() -> Any?,
        configure: BUILDER.() -> Unit
    ) {
        builderInit().apply(configure).build().getter() shouldBe expectedValue
    }
}
