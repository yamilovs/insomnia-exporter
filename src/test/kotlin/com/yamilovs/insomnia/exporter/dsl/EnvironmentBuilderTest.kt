package com.yamilovs.insomnia.exporter.dsl

import com.yamilovs.insomnia.exporter.model.BaseEnvironment
import com.yamilovs.insomnia.exporter.model.Environment
import com.yamilovs.insomnia.exporter.model.Workspace
import io.kotest.assertions.throwables.shouldThrowExactly
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe

class EnvironmentBuilderTest : StringSpec() {
    private val environmentChecker = object : ResourceChecker<Environment, EnvironmentBuilder>(
        builderInit = { EnvironmentBuilder(BaseEnvironment(Workspace())).apply { name("") } }
    ) {}

    init {
        "Check parent with DSL building" {
            val baseEnvironment = BaseEnvironment(Workspace())
            val environment = Environment.build(baseEnvironment) {
                name { "environment" }
            }

            environment.parentId shouldBe baseEnvironment.id
        }

        "Check default DSL building" {
            val environment = Environment.build(BaseEnvironment(Workspace())) {
                name { "environment" }
            }

            environment.name shouldBe "environment"
        }

        "Check that name is required for builder" {
            shouldThrowExactly<IllegalArgumentException> {
                Environment.build(BaseEnvironment(Workspace())) { }
            }
        }

        "Check name DSL methods" {
            with(environmentChecker) {
                checkDSLMethod("", { name }, {})
                checkDSLMethod("name", { name }, { name("name") })
                checkDSLMethod("name", { name }, { name { "name" } })
            }
        }

        "Check private DSL methods" {
            with(environmentChecker) {
                checkDSLMethod(false, { isPrivate }, {})
                checkDSLMethod(true, { isPrivate }, { private(true) })
                checkDSLMethod(true, { isPrivate }, { private { true } })
            }
        }

        "Check metaSortKey DSL methods" {
            with(environmentChecker) {
                checkDSLMethod(0, { metaSortKey }, {})
                checkDSLMethod(100, { metaSortKey }, { metaSortKey(100) })
                checkDSLMethod(100, { metaSortKey }, { metaSortKey { 100 } })
            }
        }

        "Check color DSL methods" {
            with(environmentChecker) {
                checkDSLMethod(null, { color }, {})
                checkDSLMethod("#FF0000", { color }, { color("#FF0000") })
                checkDSLMethod("#FF0000", { color }, { color { "#FF0000" } })
            }
        }

        "Check environment DSL methods" {
            with(environmentChecker) {
                checkDSLMethod(mapOf<String, Any>(), { data }, { })
                checkDSLMethod(mapOf("FOO" to 1), { data }, { env("FOO", 1) })
                checkDSLMethod(mapOf("FOO" to 1), { data }, { env { "FOO" to 1 } })
                checkDSLMethod(mapOf("FOO" to 1), { data }, { +("FOO" to 1) })
                checkDSLMethod(mapOf("FOO" to 1), { data }, { +Pair("FOO", 1) })
                checkDSLMethod(mapOf("FOO" to 1, "BAR" to 2), { data }, { env("FOO", 1).env("BAR", 2) })
                checkDSLMethod(mapOf("FOO" to 1, "BAR" to 2), { data }, { env("FOO", 1); env("BAR", 2) })
            }
        }
    }
}
