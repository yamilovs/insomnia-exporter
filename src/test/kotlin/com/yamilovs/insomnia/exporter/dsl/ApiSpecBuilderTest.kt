package com.yamilovs.insomnia.exporter.dsl

import com.yamilovs.insomnia.exporter.model.ApiSpec
import com.yamilovs.insomnia.exporter.model.Workspace
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe

class ApiSpecBuilderTest : StringSpec() {
    private val apiSpecChecker = object : ResourceChecker<ApiSpec, ApiSpecBuilder>(
        builderInit = { ApiSpecBuilder(Workspace("Workspace Name")) }
    ) {}

    init {
        "Check parent with DSL building" {
            val workspace = Workspace()
            val apiSpec = ApiSpec.build(workspace) { }

            apiSpec.parentId shouldBe workspace.id
        }

        "Check fileName DSL methods" {
            with(apiSpecChecker) {
                checkDSLMethod("Workspace Name", { fileName }, {})
                checkDSLMethod("Another name", { fileName }, { fileName("Another name") })
                checkDSLMethod("Another name", { fileName }, { fileName { "Another name" } })
            }
        }
    }
}
