package com.yamilovs.insomnia.exporter.dsl

import com.yamilovs.insomnia.exporter.model.*
import com.yamilovs.insomnia.exporter.model.Collection
import com.yamilovs.insomnia.exporter.model.request.FollowRedirect
import com.yamilovs.insomnia.exporter.model.request.Method
import com.yamilovs.insomnia.exporter.model.request.authentication.EmptyAuthentication
import com.yamilovs.insomnia.exporter.model.request.body.EmptyBody
import com.yamilovs.insomnia.exporter.model.request.header.Header
import com.yamilovs.insomnia.exporter.model.request.parameter.Parameter
import java.net.URL

class Examples {
    fun collection() {
        Collection.build {

            // define workspace resource
            workspace {
                // Workspace resource DSL is available here
            }

            // define apiSpec resource
            apiSpec {
                // ApiSpec resource DSL is available here
            }

            // define baseEnvironment resource
            baseEnvironment {
                // BaseEnvironment resource DSL is available here
            }

            // add request group resource
            requestGroup {
                // RequestGroup resource DSL is available here
            }

            // add request resource
            request {
                // Request resource DSL is available here
            }
        }
    }

    fun workspace() {
        Workspace.build {

            // setting up workspace name
            // next function call will rewrite previous value
            name("Workspace name")
            name { "Workspace name" }

            // setting up workspace description
            // next function call will rewrite previous value
            description("About workspace")
            description { "About workspace" }
        }
    }

    fun apiSpec() {
        val workspace = Workspace()

        ApiSpec.build(workspace) {

            // setting up fileName property
            // next function call will rewrite previous value
            fileName("About workspace")
            fileName { "About workspace" }
        }
    }

    fun baseEnvironment() {
        val collectionBuilder = CollectionBuilder()
        val workspace = Workspace()

        BaseEnvironment.build(collectionBuilder, workspace) {

            env("BASE_URL", "https://yamilovs.com") // added first env value
            env { "USERNAME" to "yamilovs" } // added second env value
            +("PASSWORD" to "qwerty") // added third env value

            // added nested environment
            environment {
                // Environment resource DSL is available here
            }
        }
    }

    fun environment() {
        val workspace = Workspace()
        val baseEnvironment = BaseEnvironment(workspace)

        Environment.build(baseEnvironment) {

            // setting up resource name
            // next function call will rewrite previous value
            name("Environment name")
            name { "Environment name" }

            env("BASE_URL", "https://yamilovs.com") // added first env value
            env { "USERNAME" to "yamilovs" } // added second env value
            +("PASSWORD" to "qwerty") // added third env value

            // setting up environment color
            // next function call will rewrite previous value
            color("#FF00FF")
            color { "#00FF00" }

            // setting private value
            // next function call will rewrite previous value
            private(true)
            private { true }

            // setting environment resource position
            // next function call will rewrite previous value
            metaSortKey(10)
            metaSortKey { 10 }
        }
    }

    fun requestGroup() {
        val collectionBuilder = CollectionBuilder()
        val workspace = Workspace()

        RequestGroup.build(collectionBuilder, workspace) {

            // setting up group name
            // next function call will rewrite previous value
            name("My group name")
            name { "My group name" }

            // setting up workspace description
            // next function call will rewrite previous value
            description("About group")
            description { "About group" }

            env("BASE_URL", "https://yamilovs.com") // added first env value
            env { "USERNAME" to "yamilovs" } // added second env value
            +("PASSWORD" to "qwerty") // added third env value

            // setting group resource position
            // next function call will rewrite previous value
            metaSortKey(10)
            metaSortKey { 10 }

            // added new nested request group
            requestGroup {
                // RequestGroup resource DSL is available here
            }

            // added new request
            request {
                // Request resource DSL is available here
            }
        }
    }

    fun request() {
        val workspace = Workspace()

        Request.build(workspace) {

            // setting up `name` property
            // next function call will rewrite previous value
            // mandatory method for Request resource
            name("My group name")
            name { "My group name" }

            // setting up `url` property
            // next function call will rewrite previous value
            // mandatory method for Request resource
            url("https://yamilovs.com")
            url { "https://yamilovs.com" }
            +URL("https://yamilovs.com")

            // setting up `description` property
            // next function call will rewrite previous value
            description("About request")
            description { "About request" }

            // setting up `method` property
            // next function call will rewrite previous value
            method(Method.POST)
            method { Method.POST }
            +Method.POST

            // setting up `body` property
            // next function call will rewrite previous value
            body(EmptyBody)
            body { EmptyBody }
            +EmptyBody

            // setting up `parameters` collection
            val emptyParameter = Parameter("first", "value")
            parameter(emptyParameter) // added first parameter
            parameter { emptyParameter.copy(name = "second") } // added second parameter
            +emptyParameter.copy(name = "third") // added third parameter
            parameters(listOf(emptyParameter)) // added all parameters from list
            parameters { listOf(emptyParameter) } // added all parameters from list

            // setting up `headers` collection
            val emptyHeader = Header("first", "value")
            header(emptyHeader) // added first header
            header { emptyHeader.copy(name = "second") } // added second header
            +emptyHeader.copy(name = "third") // added third header
            headers(listOf(emptyHeader)) // added all headers from list
            headers { listOf(emptyHeader) } // added all headers from list

            // setting up `authentication` property
            // next function call will rewrite previous value
            authentication(EmptyAuthentication)
            authentication { EmptyAuthentication }
            +EmptyAuthentication

            // setting request resource position
            // next function call will rewrite previous value
            metaSortKey(10)
            metaSortKey { 10 }

            // setting up `isPrivate` property
            // next function call will rewrite previous value
            private(true)
            private { true }

            // setting up `settingStoreCookies` property
            // next function call will rewrite previous value
            storeCookies(true)
            storeCookies { true }

            // setting up `settingSendCookies` property
            // next function call will rewrite previous value
            sendCookies(true)
            sendCookies { true }

            // setting up `settingDisableRenderRequestBody` property
            // next function call will rewrite previous value
            disableRenderRequestBody(true)
            disableRenderRequestBody { true }

            // setting up `settingEncodeUrl` property
            // next function call will rewrite previous value
            encodeUrl(true)
            encodeUrl { true }

            // setting up `settingRebuildPath` property
            // next function call will rewrite previous value
            rebuildPath(true)
            rebuildPath { true }

            // setting up `settingFollowRedirects` property
            // next function call will rewrite previous value
            followRedirects(FollowRedirect.ON)
            followRedirects { FollowRedirect.ON }
            +FollowRedirect.ON
        }
    }
}
