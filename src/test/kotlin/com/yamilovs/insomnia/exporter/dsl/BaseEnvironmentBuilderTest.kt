package com.yamilovs.insomnia.exporter.dsl

import com.yamilovs.insomnia.exporter.model.BaseEnvironment
import com.yamilovs.insomnia.exporter.model.Workspace
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe

class BaseEnvironmentBuilderTest : StringSpec() {
    private val baseEnvironmentChecker = object : ResourceChecker<BaseEnvironment, BaseEnvironmentBuilder>(
        builderInit = {
            BaseEnvironmentBuilder(
                builder = CollectionBuilder(),
                workspace = Workspace(),
            )
        }
    ) {}

    init {
        "Check parent with DSL building" {
            val workspace = Workspace()
            val baseEnvironment = BaseEnvironment.build(CollectionBuilder(), workspace) {}

            baseEnvironment.parentId shouldBe workspace.id
        }

        "Check the filling of the parens fields" {
            val collectionBuilder = CollectionBuilder()

            BaseEnvironment.build(collectionBuilder, Workspace()) {
                environment { name { "Local" } }
                environment { name { "Staging" } }
                environment { name { "Production" } }
            }

            collectionBuilder.environments.size shouldBe 3
        }

        "Check environment DSL methods" {
            with(baseEnvironmentChecker) {
                checkDSLMethod(mapOf<String, Any>(), { data }, { })
                checkDSLMethod(mapOf("FOO" to 1), { data }, { env("FOO", 1) })
                checkDSLMethod(mapOf("FOO" to 1), { data }, { env { "FOO" to 1 } })
                checkDSLMethod(mapOf("FOO" to 1), { data }, { +("FOO" to 1) })
                checkDSLMethod(mapOf("FOO" to 1), { data }, { +Pair("FOO", 1) })
                checkDSLMethod(mapOf("FOO" to 1, "BAR" to 2), { data }, { env("FOO", 1).env("BAR", 2) })
                checkDSLMethod(mapOf("FOO" to 1, "BAR" to 2), { data }, { env("FOO", 1); env("BAR", 2) })
            }
        }
    }
}
