package com.yamilovs.insomnia.exporter.dsl

import com.yamilovs.insomnia.exporter.model.*
import com.yamilovs.insomnia.exporter.model.Collection
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.shouldBe

internal class CollectionBuilderTest : StringSpec() {
    init {
        "Check default builder" {
            val collection = Collection.build { }

            val workspace = collection.firstTypedResource<Workspace>()
            val apiSpec = collection.firstTypedResource<ApiSpec>()
            val baseEnvironment = collection.firstTypedResource<BaseEnvironment>()

            baseEnvironment.parentId shouldBe workspace.id
            apiSpec.parentId shouldBe workspace.id
            apiSpec.fileName shouldBe workspace.name
            collection.resources shouldHaveSize 3 // workspace, apiSpec and baseEnvironment
        }

        "Check requestGroup DSL methods" {
            Collection.build {
                requestGroup { name("First Group") }
                requestGroup { name("Second Group") }
                requestGroup { name("Third Group") }
            }.typedResources<RequestGroup>() shouldHaveSize 3
        }

        "Check request DSL methods" {
            Collection.build {
                request { name("First Request"); url("https://yamilovs.com") }
                request { name("Second Request"); url("https://google.com") }
                request { name("Third request"); url("https://aws.com") }
            }.typedResources<Request>() shouldHaveSize 3
        }
    }

    private inline fun <reified T : IResource> Collection.firstTypedResource(): T = typedResources<T>().first()
    private inline fun <reified T : IResource> Collection.typedResources(): List<T> = resources.filterIsInstance<T>()
}
