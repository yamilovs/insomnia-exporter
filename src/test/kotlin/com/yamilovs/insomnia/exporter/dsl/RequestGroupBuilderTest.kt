package com.yamilovs.insomnia.exporter.dsl

import com.yamilovs.insomnia.exporter.model.RequestGroup
import com.yamilovs.insomnia.exporter.model.Workspace
import io.kotest.assertions.throwables.shouldThrowExactly
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.shouldContain

class RequestGroupBuilderTest : StringSpec() {
    private val requestGroupChecker = object : ResourceChecker<RequestGroup, RequestGroupBuilder>(
        builderInit = {
            RequestGroupBuilder(
                builder = CollectionBuilder(),
                parent = Workspace(),
            ).apply { name("") }
        }
    ) {}

    init {
        "Check default DSL building" {
            val group = RequestGroup.build(CollectionBuilder(), Workspace()) {
                name { "Request Group" }
            }

            group.name shouldBe "Request Group"
        }

        "Check that name is required for builder" {
            val e = shouldThrowExactly<IllegalArgumentException> {
                RequestGroup.build(CollectionBuilder(), Workspace()) { }
            }
            e.message shouldContain "name"
        }

        "Check description DSL methods" {
            with(requestGroupChecker) {
                checkDSLMethod("description", { description }, { description("description") })
                checkDSLMethod("description", { description }, { description { "description" } })
            }
        }

        "Check environment DSL methods" {
            with(requestGroupChecker) {
                checkDSLMethod(mapOf<String, Any>(), { environment }, { })
                checkDSLMethod(mapOf("FOO" to 1), { environment }, { env("FOO", 1); })
                checkDSLMethod(mapOf("FOO" to 1), { environment }, { env { "FOO" to 1 } })
                checkDSLMethod(mapOf("FOO" to 1), { environment }, { +("FOO" to 1) })
                checkDSLMethod(mapOf("FOO" to 1), { environment }, { +Pair("FOO", 1) })
                checkDSLMethod(mapOf("FOO" to 1, "BAR" to 2), { environment }, { env("FOO", 1); env("BAR", 2) })
            }
        }

        "Check requestGroup DSL methods" {
            val collectionBuilder = CollectionBuilder()

            RequestGroupBuilder(collectionBuilder, Workspace()).apply {
                name { "Parent group" }
                requestGroup { name { "Child group" } }
            }.build()

            collectionBuilder.requestGroups shouldHaveSize 1
            collectionBuilder.requestGroups.first().name shouldBe "Child group"
        }

        "Check request DSL methods" {
            val collectionBuilder = CollectionBuilder()

            RequestGroupBuilder(collectionBuilder, Workspace()).apply {
                name { "Parent group" }
                request {
                    name { "Child Request" }
                    url { "https://yamilovs.com" }
                }
            }.build()

            collectionBuilder.requests shouldHaveSize 1
            collectionBuilder.requests.first().name shouldBe "Child Request"
        }

        "Check metaSortKey DSL methods" {
            with(requestGroupChecker) {
                checkDSLMethod(0, { metaSortKey }, {})
                checkDSLMethod(100, { metaSortKey }, { metaSortKey(100) })
                checkDSLMethod(100, { metaSortKey }, { metaSortKey { 100 } })
            }
        }
    }
}
