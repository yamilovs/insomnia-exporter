package com.yamilovs.insomnia.exporter.dsl

import com.yamilovs.insomnia.exporter.model.Workspace
import io.kotest.core.spec.style.StringSpec

internal class WorkspaceBuilderTest : StringSpec() {
    private val workspaceChecker = object : ResourceChecker<Workspace, WorkspaceBuilder>(
        builderInit = { WorkspaceBuilder() }
    ) {}

    init {
        "Check name DSL methods" {
            with(workspaceChecker) {
                checkDSLMethod(Workspace.DEFAULT_NAME, { name }, {})
                checkDSLMethod("name", { name }, { name("name") })
                checkDSLMethod("name", { name }, { name { "name" } })
            }
        }

        "Check description DSL methods" {
            with(workspaceChecker) {
                checkDSLMethod("", { description }, {})
                checkDSLMethod("description", { description }, { description("description") })
                checkDSLMethod("description", { description }, { description { "description" } })
            }
        }
    }
}
