package com.yamilovs.insomnia.exporter.model.request.body

import com.yamilovs.insomnia.exporter.Jackson
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe

internal class JsonBodyTest : StringSpec() {
    init {
        "Check that serialization is correct" {
            val jackson = Jackson.default
            val json = jackson.readTree(jackson.writeValueAsString(JsonBody("Text")))

            json.has("text") shouldBe true
            json.get("text").isTextual shouldBe true
            json.get("text").asText() shouldBe "Text"

            json.has("mimeType") shouldBe true
            json.get("mimeType").isTextual shouldBe true
            json.get("mimeType").asText() shouldBe "application/json"
        }
    }
}