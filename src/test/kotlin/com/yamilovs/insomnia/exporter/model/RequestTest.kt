package com.yamilovs.insomnia.exporter.model

import com.yamilovs.insomnia.exporter.Jackson
import com.yamilovs.insomnia.exporter.model.request.FollowRedirect
import com.yamilovs.insomnia.exporter.model.request.Method
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.shouldStartWith

internal class RequestTest : StringSpec() {
    init {
        "Check that serialization of the base parameters is correct" {
            val jackson = Jackson.default
            val workspace = Workspace("")
            val request = Request(
                parent = workspace,
                url = "https://hello.world",
                name = "Request Name",
            )
            val json = jackson.readTree(jackson.writeValueAsString(request))

            json.has("requestParent") shouldBe false

            json.has("_id") shouldBe true
            json.get("_id").isTextual shouldBe true
            json.get("_id").asText() shouldStartWith Type.REQUEST.prefix

            json.has("_type") shouldBe true
            json.get("_type").isTextual shouldBe true
            json.get("_type").asText() shouldBe Type.REQUEST.name.lowercase()

            json.has("url") shouldBe true
            json.get("url").isTextual shouldBe true
            json.get("url").asText() shouldBe "https://hello.world"

            json.has("name") shouldBe true
            json.get("name").isTextual shouldBe true
            json.get("name").asText() shouldBe "Request Name"

            json.has("description") shouldBe true
            json.get("description").isTextual shouldBe true
            json.get("description").asText() shouldBe ""

            json.has("method") shouldBe true
            json.get("method").isTextual shouldBe true
            json.get("method").asText() shouldBe Method.GET.toString()

            json.has("body") shouldBe true
            json.get("body").isObject shouldBe true
            json.get("body").asIterable() shouldHaveSize 0

            json.has("parameters") shouldBe true
            json.get("parameters").isArray shouldBe true
            json.get("parameters").asIterable() shouldHaveSize 0

            json.has("headers") shouldBe true
            json.get("headers").isArray shouldBe true
            json.get("headers").asIterable() shouldHaveSize 0

            json.has("authentication") shouldBe true
            json.get("authentication").isObject shouldBe true
            json.get("authentication").asIterable() shouldHaveSize 0

            json.has("metaSortKey") shouldBe true
            json.get("metaSortKey").isInt shouldBe true
            json.get("metaSortKey").asInt() shouldBe 0

            json.has("isPrivate") shouldBe true
            json.get("isPrivate").isBoolean shouldBe true
            json.get("isPrivate").asBoolean() shouldBe false

            json.has("settingStoreCookies") shouldBe true
            json.get("settingStoreCookies").isBoolean shouldBe true
            json.get("settingStoreCookies").asBoolean() shouldBe true

            json.has("settingSendCookies") shouldBe true
            json.get("settingSendCookies").isBoolean shouldBe true
            json.get("settingSendCookies").asBoolean() shouldBe true

            json.has("settingDisableRenderRequestBody") shouldBe true
            json.get("settingDisableRenderRequestBody").isBoolean shouldBe true
            json.get("settingDisableRenderRequestBody").asBoolean() shouldBe false

            json.has("settingEncodeUrl") shouldBe true
            json.get("settingEncodeUrl").isBoolean shouldBe true
            json.get("settingEncodeUrl").asBoolean() shouldBe true

            json.has("settingRebuildPath") shouldBe true
            json.get("settingRebuildPath").isBoolean shouldBe true
            json.get("settingRebuildPath").asBoolean() shouldBe true

            json.has("settingFollowRedirects") shouldBe true
            json.get("settingFollowRedirects").isTextual shouldBe true
            json.get("settingFollowRedirects").asText() shouldBe FollowRedirect.GLOBAL.toString()

            json.has("parentId") shouldBe true
            json.get("parentId").isTextual shouldBe true
            json.get("parentId").asText() shouldBe workspace.id
        }

        "Check that request can be in request group" {
            val workspace = Workspace("")
            val group = RequestGroup(parent = workspace, name = "Group")
            val request = Request(
                parent = group,
                url = "https://hello.world",
                name = "Request Name",
            )

            request.parentId shouldBe group.id
        }
    }
}
