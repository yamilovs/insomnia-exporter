package com.yamilovs.insomnia.exporter.model.request.body

import com.yamilovs.insomnia.exporter.Jackson
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe

internal class EmptyBodyTest : StringSpec() {
    init {
        "Check that serialization object is empty" {
            val jackson = Jackson.default
            val json = jackson.readTree(jackson.writeValueAsString(EmptyBody))

            json.has("mimeType") shouldBe false
        }
    }
}