package com.yamilovs.insomnia.exporter.model.request.header.content_type

import com.yamilovs.insomnia.exporter.Jackson
import com.yamilovs.insomnia.exporter.model.Type
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.shouldStartWith
import java.nio.charset.Charset

internal class JsonContentTypeHeaderTest : StringSpec() {
    init {
        "Check that serialization is correct" {
            val jackson = Jackson.default
            val json = jackson.readTree(jackson.writeValueAsString(JsonContentTypeHeader()))

            json.has("id") shouldBe true
            json.get("id").isTextual shouldBe true
            json.get("id").asText() shouldStartWith Type.PAIR.prefix

            json.has("name") shouldBe true
            json.get("name").isTextual shouldBe true
            json.get("name").asText() shouldBe "Content-Type"

            json.has("value") shouldBe true
            json.get("value").isTextual shouldBe true
            json.get("value").asText() shouldBe "application/json; charset=utf-8"

            json.has("description") shouldBe true
            json.get("description").isTextual shouldBe true

            json.has("disabled") shouldBe true
            json.get("disabled").isBoolean shouldBe true
            json.get("disabled").asBoolean() shouldBe false
        }

        "Check header with different charset" {
            val header = JsonContentTypeHeader(charset = Charset.forName("WINDOWS-1251"))

            header.value shouldBe "application/json; charset=windows-1251"
        }
    }
}