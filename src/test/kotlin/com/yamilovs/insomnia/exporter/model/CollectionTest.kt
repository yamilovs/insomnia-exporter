package com.yamilovs.insomnia.exporter.model

import com.yamilovs.insomnia.exporter.Jackson
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.shouldNotBeEmpty

internal class CollectionTest : StringSpec() {
    private val supportedExportFormatVersion = 4

    init {
        "Check that serialization is correct" {
            val jackson = Jackson.default
            val workspace = Workspace("")
            val json = jackson.readTree(
                jackson.writeValueAsString(
                    Collection(
                        workspace = workspace,
                        apiSpec = ApiSpec(workspace, ""),
                        baseEnvironment = BaseEnvironment(workspace)
                    )
                )
            )

            json.has("_type") shouldBe true
            json.get("_type").isTextual shouldBe true
            json.get("_type").asText() shouldBe Type.EXPORT.name.lowercase()

            json.has("__export_format") shouldBe true
            json.get("__export_format").isInt shouldBe true
            json.get("__export_format").asInt() shouldBe supportedExportFormatVersion

            json.has("__export_date") shouldBe true
            json.get("__export_date").isTextual shouldBe true
            json.get("__export_date").asText().shouldNotBeEmpty()

            json.has("__export_source") shouldBe true
            json.get("__export_source").isTextual shouldBe true
            json.get("__export_source").asText().shouldNotBeEmpty()

            json.has("resources") shouldBe true
            json.get("resources").isArray shouldBe true
            json.get("resources").asIterable() shouldHaveSize 3 // workspace, apiSpec and baseEnvironment
        }
    }
}
