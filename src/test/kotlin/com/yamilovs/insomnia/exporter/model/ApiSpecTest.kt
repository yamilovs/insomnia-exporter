package com.yamilovs.insomnia.exporter.model

import com.yamilovs.insomnia.exporter.Jackson
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.shouldStartWith

internal class ApiSpecTest : StringSpec() {
    init {
        "Check that serialization is correct" {
            val jackson = Jackson.default
            val workspace = Workspace("")
            val apiSpec = ApiSpec(workspace = workspace, fileName = "file name")
            val json = jackson.readTree(jackson.writeValueAsString(apiSpec))

            json.has("workspace") shouldBe false

            json.has("_id") shouldBe true
            json.get("_id").isTextual shouldBe true
            json.get("_id").asText() shouldStartWith Type.API_SPEC.prefix

            json.has("_type") shouldBe true
            json.get("_type").isTextual shouldBe true
            json.get("_type").asText() shouldBe Type.API_SPEC.name.lowercase()

            json.has("fileName") shouldBe true
            json.get("fileName").isTextual shouldBe true
            json.get("fileName").asText() shouldBe "file name"

            json.has("contents") shouldBe true
            json.get("contents").isTextual shouldBe true
            json.get("contents").asText() shouldBe ""

            json.has("contentType") shouldBe true
            json.get("contentType").isTextual shouldBe true
            json.get("contentType").asText() shouldBe "yaml"

            json.has("modified") shouldBe true
            json.get("modified").isLong shouldBe true

            json.has("created") shouldBe true
            json.get("created").isLong shouldBe true

            json.has("parentId") shouldBe true
            json.get("parentId").isTextual shouldBe true
            json.get("parentId").asText() shouldBe workspace.id
        }
    }
}
