package com.yamilovs.insomnia.exporter.model

import com.fasterxml.jackson.databind.node.ObjectNode
import com.yamilovs.insomnia.exporter.Jackson
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.shouldStartWith

internal class RequestGroupTest : StringSpec() {
    private val jackson = Jackson.default

    init {
        "Check that serialization is correct" {
            val workspace = Workspace("")
            val requestGroup = RequestGroup(
                parent = workspace,
                name = "Request Group",
                description = "Group Description",
                metaSortKey = 1,
            )
            val json = jackson.readTree(jackson.writeValueAsString(requestGroup))

            json.has("workspace") shouldBe false

            json.has("_id") shouldBe true
            json.get("_id").isTextual shouldBe true
            json.get("_id").asText() shouldStartWith Type.REQUEST_GROUP.prefix

            json.has("_type") shouldBe true
            json.get("_type").isTextual shouldBe true
            json.get("_type").asText() shouldBe Type.REQUEST_GROUP.name.lowercase()

            json.has("name") shouldBe true
            json.get("name").isTextual shouldBe true
            json.get("name").asText() shouldBe "Request Group"

            json.has("description") shouldBe true
            json.get("description").isTextual shouldBe true
            json.get("description").asText() shouldBe "Group Description"

            json.has("metaSortKey") shouldBe true
            json.get("metaSortKey").isInt shouldBe true
            json.get("metaSortKey").asInt() shouldBe 1

            json.has("environment") shouldBe true
            json.get("environment").isObject shouldBe true
            json.get("environment") shouldHaveSize 0

            json.has("environmentPropertyOrder") shouldBe true
            json.get("environmentPropertyOrder").isNull shouldBe true

            json.has("modified") shouldBe true
            json.get("modified").isLong shouldBe true

            json.has("created") shouldBe true
            json.get("created").isLong shouldBe true

            json.has("parentId") shouldBe true
            json.get("parentId").isTextual shouldBe true
            json.get("parentId").asText() shouldBe workspace.id
        }

        "Check that group can contains another group" {
            val workspace = Workspace("")
            val group1 = RequestGroup(parent = workspace, name = "Group1")
            val group2 = RequestGroup(parent = group1, name = "Group2")

            group1.parentId shouldBe workspace.id
            group2.parentId shouldBe group1.id
        }

        "Check environment and environmentPropertyOrder serialization" {
            val requestGroup = RequestGroup(
                parent = Workspace(""),
                name = "",
                environment = mapOf(
                    "1" to 1,
                    "2" to mapOf("2_1" to 21, "2_2" to 22),
                    "3" to "3",
                )
            )
            val json = jackson.readTree(jackson.writeValueAsString(requestGroup))
            val data = json.get("environment").asIterable() as ObjectNode
            val order = json.get("environmentPropertyOrder").asIterable() as ObjectNode

            data.size() shouldBe 3
            data.get("1").isInt shouldBe true
            data.get("2").isObject shouldBe true
            data.get("3").isTextual shouldBe true

            order.size() shouldBe 2
            order.get("&").isArray shouldBe true
            order.get("&").map { it.asText() } shouldBe listOf("1", "2", "3")
            order.get("&~|2").isArray shouldBe true
            order.get("&~|2").map { it.asText() } shouldBe listOf("2_1", "2_2")
        }
    }
}
