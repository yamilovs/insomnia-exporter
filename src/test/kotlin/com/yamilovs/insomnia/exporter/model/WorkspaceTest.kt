package com.yamilovs.insomnia.exporter.model

import com.yamilovs.insomnia.exporter.Jackson
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.shouldStartWith

internal class WorkspaceTest : StringSpec() {
    init {
        "Check that serialization is correct" {
            val jackson = Jackson.default
            val workspace = Workspace(name = "name", description = "description")
            val json = jackson.readTree(jackson.writeValueAsString(workspace))

            json.has("_id") shouldBe true
            json.get("_id").isTextual shouldBe true
            json.get("_id").asText() shouldStartWith Type.WORKSPACE.prefix

            json.has("_type") shouldBe true
            json.get("_type").isTextual shouldBe true
            json.get("_type").asText() shouldBe Type.WORKSPACE.name.lowercase()

            json.has("name") shouldBe true
            json.get("name").isTextual shouldBe true
            json.get("name").asText() shouldBe "name"

            json.has("description") shouldBe true
            json.get("description").isTextual shouldBe true
            json.get("description").asText() shouldBe "description"

            json.has("scope") shouldBe true
            json.get("scope").isTextual shouldBe true
            json.get("scope").asText() shouldBe Scope.COLLECTION.name.lowercase()

            json.has("parentId") shouldBe true
            json.get("parentId").isNull shouldBe true

            json.has("modified") shouldBe true
            json.get("modified").isLong shouldBe true

            json.has("created") shouldBe true
            json.get("created").isLong shouldBe true
        }
    }
}
