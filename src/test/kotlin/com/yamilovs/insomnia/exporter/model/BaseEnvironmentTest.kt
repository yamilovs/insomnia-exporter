package com.yamilovs.insomnia.exporter.model

import com.fasterxml.jackson.databind.node.ObjectNode
import com.yamilovs.insomnia.exporter.Jackson
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.shouldStartWith

internal class BaseEnvironmentTest : StringSpec() {
    private val jackson = Jackson.default

    init {
        "Check that serialization is correct" {
            val workspace = Workspace("")
            val baseEnvironment = BaseEnvironment(workspace = workspace)
            val json = jackson.readTree(jackson.writeValueAsString(baseEnvironment))

            json.has("workspace") shouldBe false

            json.has("_id") shouldBe true
            json.get("_id").isTextual shouldBe true
            json.get("_id").asText() shouldStartWith Type.ENVIRONMENT.prefix

            json.has("_type") shouldBe true
            json.get("_type").isTextual shouldBe true
            json.get("_type").asText() shouldBe Type.ENVIRONMENT.name.lowercase()

            json.has("name") shouldBe true
            json.get("name").isTextual shouldBe true
            json.get("name").asText() shouldBe "Base Environment"

            json.has("color") shouldBe true
            json.get("color").isNull shouldBe true

            json.has("isPrivate") shouldBe true
            json.get("isPrivate").isBoolean shouldBe true
            json.get("isPrivate").asBoolean() shouldBe false

            json.has("metaSortKey") shouldBe true
            json.get("metaSortKey").isInt shouldBe true
            json.get("metaSortKey").asInt() shouldBe 0

            json.has("data") shouldBe true
            json.get("data").isObject shouldBe true
            json.get("data") shouldHaveSize 0

            json.has("dataPropertyOrder") shouldBe true
            json.get("dataPropertyOrder").isNull shouldBe true

            json.has("modified") shouldBe true
            json.get("modified").isLong shouldBe true

            json.has("created") shouldBe true
            json.get("created").isLong shouldBe true

            json.has("parentId") shouldBe true
            json.get("parentId").isTextual shouldBe true
            json.get("parentId").asText() shouldBe workspace.id
        }

        "Check data and dataPropertyOrder serialization" {
            val baseEnvironment = BaseEnvironment(
                workspace = Workspace(""),
                data = mapOf(
                    "1" to 1,
                    "2" to mapOf("2_1" to 21, "2_2" to 22),
                    "3" to "3",
                )
            )
            val json = jackson.readTree(jackson.writeValueAsString(baseEnvironment))
            val data = json.get("data").asIterable() as ObjectNode
            val order = json.get("dataPropertyOrder").asIterable() as ObjectNode

            data.size() shouldBe 3
            data.get("1").isInt shouldBe true
            data.get("2").isObject shouldBe true
            data.get("3").isTextual shouldBe true

            order.size() shouldBe 2
            order.get("&").isArray shouldBe true
            order.get("&").map { it.asText() } shouldBe listOf("1", "2", "3")
            order.get("&~|2").isArray shouldBe true
            order.get("&~|2").map { it.asText() } shouldBe listOf("2_1", "2_2")
        }
    }
}
