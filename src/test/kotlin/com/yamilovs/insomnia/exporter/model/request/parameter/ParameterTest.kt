package com.yamilovs.insomnia.exporter.model.request.parameter

import com.yamilovs.insomnia.exporter.Jackson
import com.yamilovs.insomnia.exporter.model.Type
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.shouldStartWith

internal class ParameterTest : StringSpec() {
    init {
        "Check that serialization is correct" {
            val jackson = Jackson.default
            val json = jackson.readTree(
                jackson.writeValueAsString(
                    Parameter(
                        name = "Parameter",
                        value = "Value",
                        description = "Description",
                        disabled = true,
                    )
                )
            )

            json.has("id") shouldBe true
            json.get("id").isTextual shouldBe true
            json.get("id").asText() shouldStartWith Type.PAIR.prefix

            json.has("name") shouldBe true
            json.get("name").isTextual shouldBe true
            json.get("name").asText() shouldBe "Parameter"

            json.has("value") shouldBe true
            json.get("value").isTextual shouldBe true
            json.get("value").asText() shouldBe "Value"

            json.has("description") shouldBe true
            json.get("description").isTextual shouldBe true
            json.get("description").asText() shouldBe "Description"

            json.has("disabled") shouldBe true
            json.get("disabled").isBoolean shouldBe true
            json.get("disabled").asBoolean() shouldBe true
        }
    }
}
