package com.yamilovs.insomnia.exporter.model.request.authentication

import com.yamilovs.insomnia.exporter.Jackson
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe

internal class EmptyAuthenticationTest : StringSpec() {
    init {
        "Check that serialization object is empty" {
            val jackson = Jackson.default
            val json = jackson.readTree(jackson.writeValueAsString(EmptyAuthentication))

            json.has("type") shouldBe false
        }
    }
}