package com.yamilovs.insomnia.exporter.dsl

import com.yamilovs.insomnia.exporter.model.Workspace

/**
 * Workspace resource builder
 * @sample com.yamilovs.insomnia.exporter.dsl.Examples.workspace
 */
@ResourceBuilder
class WorkspaceBuilder : IResourceBuilder<Workspace> {
    private var _name: String = Workspace.DEFAULT_NAME
    private var _description: String = ""

    override fun build(): Workspace = Workspace(
        name = _name,
        description = _description,
    )

    fun name(name: String): WorkspaceBuilder {
        _name = name

        return this
    }

    fun name(init: () -> String) = name(init())

    fun description(description: String): WorkspaceBuilder {
        _description = description

        return this
    }

    fun description(init: () -> String) = description(init())
}
