package com.yamilovs.insomnia.exporter.dsl

import com.yamilovs.insomnia.exporter.model.IRequestGroupParent
import com.yamilovs.insomnia.exporter.model.Request
import com.yamilovs.insomnia.exporter.model.RequestGroup

/**
 * RequestGroup resource builder
 * @sample com.yamilovs.insomnia.exporter.dsl.Examples.requestGroup
 */
@ResourceBuilder
class RequestGroupBuilder(
    private val builder: CollectionBuilder,
    private val parent: IRequestGroupParent
) : IResourceBuilder<RequestGroup> {
    private lateinit var _name: String

    private val requestGroupBuilders: MutableList<(RequestGroup) -> RequestGroup> = mutableListOf()
    private val requestBuilders: MutableList<(RequestGroup) -> Request> = mutableListOf()

    private var _description: String = ""
    private var _environment: MutableMap<String, Any> = mutableMapOf()
    private var _metaSortKey: Long = 0

    override fun build(): RequestGroup {
        require(this::_name.isInitialized) {
            "You must to setting up the name of the Request Group"
        }

        return RequestGroup(
            parent = parent,
            name = _name,
            description = _description,
            environment = _environment,
            metaSortKey = _metaSortKey,
        ).also { group ->
            builder.requestGroups.addAll(
                requestGroupBuilders.map { it(group) }
            )
            builder.requests.addAll(
                requestBuilders.map { it(group) }
            )
        }
    }

    fun name(name: String): RequestGroupBuilder {
        _name = name

        return this
    }

    fun name(init: () -> String) = name(init())

    fun description(description: String): RequestGroupBuilder {
        _description = description

        return this
    }

    fun description(init: () -> String) = description(init())

    fun env(key: String, value: Any): RequestGroupBuilder {
        _environment[key] = value

        return this
    }

    fun env(init: () -> Pair<String, Any>) = init().let { env(it.first, it.second) }

    operator fun Pair<String, Any>.unaryPlus() = env(first, second)

    fun requestGroup(init: RequestGroupBuilder.() -> Unit): RequestGroupBuilder {
        requestGroupBuilders.add { requestGroup -> RequestGroup.build(builder, requestGroup, init) }

        return this
    }

    fun metaSortKey(metaSortKey: Long): RequestGroupBuilder {
        _metaSortKey = metaSortKey

        return this
    }

    fun metaSortKey(init: () -> Long) = metaSortKey(init())

    fun request(configure: RequestBuilder.() -> Unit): RequestGroupBuilder {
        requestBuilders.add { requestGroup -> Request.build(requestGroup, configure) }

        return this
    }
}
