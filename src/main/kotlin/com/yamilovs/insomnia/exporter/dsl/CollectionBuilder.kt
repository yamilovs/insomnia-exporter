package com.yamilovs.insomnia.exporter.dsl

import com.yamilovs.insomnia.exporter.model.ApiSpec
import com.yamilovs.insomnia.exporter.model.BaseEnvironment
import com.yamilovs.insomnia.exporter.model.Collection
import com.yamilovs.insomnia.exporter.model.Environment
import com.yamilovs.insomnia.exporter.model.Request
import com.yamilovs.insomnia.exporter.model.RequestGroup
import com.yamilovs.insomnia.exporter.model.Workspace

/**
 * Collection resource builder
 * @sample com.yamilovs.insomnia.exporter.dsl.Examples.collection
 */
class CollectionBuilder {
    private lateinit var _workspace: () -> Workspace
    private lateinit var _apiSpec: (Workspace) -> ApiSpec
    private lateinit var _baseEnvironment: (Workspace) -> BaseEnvironment

    private val requestGroupBuilders: MutableList<(Workspace) -> RequestGroup> = mutableListOf()
    private val requestBuilders: MutableList<(Workspace) -> Request> = mutableListOf()

    val environments: MutableList<Environment> = mutableListOf()
    val requestGroups: MutableList<RequestGroup> = mutableListOf()
    val requests: MutableList<Request> = mutableListOf()

    fun build(): Collection {
        initDefaults()

        val workspace = _workspace()

        return Collection(
            workspace = workspace,
            apiSpec = _apiSpec(workspace),
            baseEnvironment = _baseEnvironment(workspace),
            environments = environments,
            requestGroups = requestGroupBuilders.map { it(workspace) } + requestGroups,
            requests = requestBuilders.map { it(workspace) } + requests,
        )
    }

    fun workspace(configure: WorkspaceBuilder.() -> Unit = {}): CollectionBuilder {
        _workspace = { Workspace.build(configure) }

        return this
    }

    fun apiSpec(configure: ApiSpecBuilder.() -> Unit = {}): CollectionBuilder {
        _apiSpec = { workspace -> ApiSpec.build(workspace, configure) }

        return this
    }

    fun baseEnvironment(configure: BaseEnvironmentBuilder.() -> Unit = {}): CollectionBuilder {
        _baseEnvironment = { workspace -> BaseEnvironment.build(this, workspace, configure) }

        return this
    }

    fun requestGroup(configure: RequestGroupBuilder.() -> Unit): CollectionBuilder {
        requestGroupBuilders.add { workspace -> RequestGroup.build(this, workspace, configure) }

        return this
    }

    fun request(configure: RequestBuilder.() -> Unit): CollectionBuilder {
        requestBuilders.add { workspace -> Request.build(workspace, configure) }

        return this
    }

    private fun initDefaults() {
        if (!this::_workspace.isInitialized) {
            workspace()
        }
        if (!this::_apiSpec.isInitialized) {
            apiSpec()
        }
        if (!this::_baseEnvironment.isInitialized) {
            baseEnvironment()
        }
    }
}
