package com.yamilovs.insomnia.exporter.dsl

import com.yamilovs.insomnia.exporter.model.BaseEnvironment
import com.yamilovs.insomnia.exporter.model.Environment
import com.yamilovs.insomnia.exporter.model.Workspace

/**
 * BaseEnvironment resource builder
 * @sample com.yamilovs.insomnia.exporter.dsl.Examples.baseEnvironment
 */
@ResourceBuilder
class BaseEnvironmentBuilder(
    private val builder: CollectionBuilder,
    private val workspace: Workspace
) : IResourceBuilder<BaseEnvironment> {
    private var _data: MutableMap<String, Any> = mutableMapOf()

    private val environmentBuilders: MutableList<(BaseEnvironment) -> Environment> = mutableListOf()

    override fun build() = BaseEnvironment(
        workspace = workspace,
        data = _data,
    ).also { base ->
        builder.environments.addAll(
            environmentBuilders.map { it(base) }
        )
    }

    fun env(key: String, value: Any): BaseEnvironmentBuilder {
        _data[key] = value

        return this
    }

    fun env(init: () -> Pair<String, Any>) = init().let { env(it.first, it.second) }

    operator fun Pair<String, Any>.unaryPlus() = env(first, second)

    fun environment(configure: EnvironmentBuilder.() -> Unit = {}): BaseEnvironmentBuilder {
        environmentBuilders.add { Environment.build(it, configure) }

        return this
    }
}
