package com.yamilovs.insomnia.exporter.dsl

import com.yamilovs.insomnia.exporter.model.IRequestParent
import com.yamilovs.insomnia.exporter.model.Request
import com.yamilovs.insomnia.exporter.model.request.FollowRedirect
import com.yamilovs.insomnia.exporter.model.request.Method
import com.yamilovs.insomnia.exporter.model.request.authentication.EmptyAuthentication
import com.yamilovs.insomnia.exporter.model.request.authentication.IAuthentication
import com.yamilovs.insomnia.exporter.model.request.body.EmptyBody
import com.yamilovs.insomnia.exporter.model.request.body.IBody
import com.yamilovs.insomnia.exporter.model.request.header.IHeader
import com.yamilovs.insomnia.exporter.model.request.parameter.IParameter
import java.net.URL

/**
 * Request resource builder
 * @sample com.yamilovs.insomnia.exporter.dsl.Examples.request
 */
@ResourceBuilder
class RequestBuilder(private val parent: IRequestParent) : IResourceBuilder<Request> {
    private lateinit var _url: String
    private lateinit var _name: String

    private var _description: String = ""
    private var _method: Method = Method.GET
    private var _body: IBody = EmptyBody
    private val _parameters: MutableList<IParameter> = mutableListOf()
    private val _headers: MutableList<IHeader> = mutableListOf()
    private var _authentication: IAuthentication = EmptyAuthentication
    private var _metaSortKey: Long = 0
    private var _isPrivate: Boolean = false
    private var _settingStoreCookies: Boolean = true
    private var _settingSendCookies: Boolean = true
    private var _settingDisableRenderRequestBody: Boolean = false
    private var _settingEncodeUrl: Boolean = true
    private var _settingRebuildPath: Boolean = true
    private var _settingFollowRedirects: FollowRedirect = FollowRedirect.GLOBAL

    override fun build(): Request {
        requireNecessary()

        return Request(
            parent = parent,
            url = _url,
            name = _name,
            description = _description,
            method = _method,
            body = _body,
            parameters = _parameters,
            headers = _headers,
            authentication = _authentication,
            metaSortKey = _metaSortKey,
            isPrivate = _isPrivate,
            settingStoreCookies = _settingStoreCookies,
            settingSendCookies = _settingSendCookies,
            settingDisableRenderRequestBody = _settingDisableRenderRequestBody,
            settingEncodeUrl = _settingEncodeUrl,
            settingRebuildPath = _settingRebuildPath,
            settingFollowRedirects = _settingFollowRedirects,
        )
    }

    fun name(name: String): RequestBuilder {
        _name = name

        return this
    }

    fun name(init: () -> String) = name(init())

    fun url(url: String): RequestBuilder {
        _url = url

        return this
    }

    fun url(init: () -> String) = url(init())

    operator fun URL.unaryPlus() = url(toExternalForm())

    fun description(description: String): RequestBuilder {
        _description = description

        return this
    }

    fun description(init: () -> String) = description(init())

    fun method(method: Method): RequestBuilder {
        _method = method

        return this
    }

    fun method(init: () -> Method) = method(init())

    operator fun Method.unaryPlus() = method(this)

    fun body(body: IBody): RequestBuilder {
        _body = body

        return this
    }

    fun body(init: () -> IBody) = body(init())

    operator fun IBody.unaryPlus() = body(this)

    fun parameter(parameter: IParameter): RequestBuilder {
        _parameters.add(parameter)

        return this
    }

    fun parameter(init: () -> IParameter) = parameter(init())

    operator fun IParameter.unaryPlus() = parameter(this)

    fun parameters(parameters: List<IParameter>): RequestBuilder {
        _parameters.addAll(parameters)

        return this
    }

    fun parameters(init: () -> List<IParameter>) = parameters(init())

    fun header(header: IHeader): RequestBuilder {
        _headers.add(header)

        return this
    }

    fun header(init: () -> IHeader) = header(init())

    operator fun IHeader.unaryPlus() = header(this)

    fun headers(headers: List<IHeader>): RequestBuilder {
        _headers.addAll(headers)

        return this
    }

    fun headers(init: () -> List<IHeader>) = headers(init())

    fun authentication(authentication: IAuthentication): RequestBuilder {
        _authentication = authentication

        return this
    }

    fun authentication(init: () -> IAuthentication) = authentication(init())

    operator fun IAuthentication.unaryPlus() = authentication(this)

    fun metaSortKey(metaSortKey: Long): RequestBuilder {
        _metaSortKey = metaSortKey

        return this
    }

    fun metaSortKey(init: () -> Long) = metaSortKey(init())

    fun private(isPrivate: Boolean): RequestBuilder {
        _isPrivate = isPrivate

        return this
    }

    fun private(init: () -> Boolean) = private(init())

    fun storeCookies(storeCookies: Boolean): RequestBuilder {
        _settingStoreCookies = storeCookies

        return this
    }

    fun storeCookies(init: () -> Boolean) = storeCookies(init())

    fun sendCookies(sendCookies: Boolean): RequestBuilder {
        _settingSendCookies = sendCookies

        return this
    }

    fun sendCookies(init: () -> Boolean) = sendCookies(init())

    fun disableRenderRequestBody(disableRenderRequestBody: Boolean): RequestBuilder {
        _settingDisableRenderRequestBody = disableRenderRequestBody

        return this
    }

    fun disableRenderRequestBody(init: () -> Boolean) = disableRenderRequestBody(init())

    fun encodeUrl(encodeUrl: Boolean): RequestBuilder {
        _settingEncodeUrl = encodeUrl

        return this
    }

    fun encodeUrl(init: () -> Boolean) = encodeUrl(init())

    fun rebuildPath(rebuildPath: Boolean): RequestBuilder {
        _settingRebuildPath = rebuildPath

        return this
    }

    fun rebuildPath(init: () -> Boolean) = rebuildPath(init())

    fun followRedirects(followRedirects: FollowRedirect): RequestBuilder {
        _settingFollowRedirects = followRedirects

        return this
    }

    fun followRedirects(init: () -> FollowRedirect) = followRedirects(init())

    operator fun FollowRedirect.unaryPlus() = followRedirects(this)

    private fun requireNecessary() {
        require(this::_url.isInitialized) {
            "You must to setting up the url of the Request"
        }
        require(this::_name.isInitialized) {
            "You must to setting up the name of the Request"
        }
    }
}
