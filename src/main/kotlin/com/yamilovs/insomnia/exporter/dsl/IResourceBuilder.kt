package com.yamilovs.insomnia.exporter.dsl

import com.yamilovs.insomnia.exporter.model.IResource

interface IResourceBuilder<RESOURCE : IResource> {
    fun build(): RESOURCE
}