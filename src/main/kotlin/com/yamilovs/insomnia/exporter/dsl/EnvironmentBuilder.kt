package com.yamilovs.insomnia.exporter.dsl

import com.yamilovs.insomnia.exporter.model.BaseEnvironment
import com.yamilovs.insomnia.exporter.model.Environment

/**
 * Environment resource builder
 * @sample com.yamilovs.insomnia.exporter.dsl.Examples.environment
 */
@ResourceBuilder
class EnvironmentBuilder(private val baseEnvironment: BaseEnvironment) : IResourceBuilder<Environment> {
    private lateinit var _name: String
    private var _data: MutableMap<String, Any> = mutableMapOf()
    private var _color: String? = null
    private var _isPrivate: Boolean = false
    private var _metaSortKey: Long = 0

    override fun build(): Environment {
        require(this::_name.isInitialized) {
            "You must to setting up the name of the Environment"
        }

        return Environment(
            baseEnvironment = baseEnvironment,
            name = _name,
            data = _data,
            color = _color,
            isPrivate = _isPrivate,
            metaSortKey = _metaSortKey,
        )
    }

    fun name(name: String): EnvironmentBuilder {
        _name = name

        return this
    }

    fun name(init: () -> String) = name(init())


    fun env(key: String, value: Any): EnvironmentBuilder {
        _data[key] = value

        return this
    }

    fun env(init: () -> Pair<String, Any>) = init().let { env(it.first, it.second) }

    operator fun Pair<String, Any>.unaryPlus() = env(first, second)

    fun color(color: String): EnvironmentBuilder {
        _color = color

        return this
    }

    fun color(init: () -> String) = color(init())

    fun private(isPrivate: Boolean): EnvironmentBuilder {
        _isPrivate = isPrivate

        return this
    }

    fun private(init: () -> Boolean) = private(init())

    fun metaSortKey(metaSortKey: Long): EnvironmentBuilder {
        _metaSortKey = metaSortKey

        return this
    }

    fun metaSortKey(init: () -> Long) = metaSortKey(init())
}
