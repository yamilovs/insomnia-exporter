package com.yamilovs.insomnia.exporter.dsl

import com.yamilovs.insomnia.exporter.model.ApiSpec
import com.yamilovs.insomnia.exporter.model.Workspace

/**
 * ApiSpec resource builder
 * @sample com.yamilovs.insomnia.exporter.dsl.Examples.apiSpec
 */
@ResourceBuilder
class ApiSpecBuilder(private val workspace: Workspace) : IResourceBuilder<ApiSpec> {
    private var _fileName: String = workspace.name

    override fun build(): ApiSpec = ApiSpec(
        workspace = workspace,
        fileName = _fileName,
    )

    fun fileName(fileName: String): ApiSpecBuilder {
        _fileName = fileName

        return this
    }

    fun fileName(init: () -> String) = fileName(init())
}
