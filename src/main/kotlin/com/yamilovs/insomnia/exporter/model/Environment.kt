package com.yamilovs.insomnia.exporter.model

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import com.yamilovs.insomnia.exporter.dsl.EnvironmentBuilder
import com.yamilovs.insomnia.exporter.utils.PropertyOrder

/**
 * Environment resource
 */
data class Environment(
    /**
     * Parent of this Environment resource
     */
    @JsonIgnore
    val baseEnvironment: BaseEnvironment,

    /**
     * Environment name
     */
    @get:JsonProperty("name")
    val name: String,

    /**
     * Collection of properties
     */
    @get:JsonProperty("data")
    val data: Map<String, Any> = mapOf(),

    /**
     * Assigned color for this Environment
     */
    @get:JsonProperty("color")
    val color: String? = null,

    /**
     * Is this environment private.
     * Private environments will not be exported or synced via Insomnia
     */
    @get:JsonProperty("isPrivate")
    val isPrivate: Boolean = false,

    /**
     * The "weight" of this resource
     */
    @get:JsonProperty("metaSortKey")
    val metaSortKey: Long = 0,
) : ResourceBase(Type.ENVIRONMENT, baseEnvironment.id) {

    /**
     * Mandatory structure field.
     * Calculated automatically based on `data` property
     * @see data
     */
    @get:JsonProperty("dataPropertyOrder")
    val dataPropertyOrder = PropertyOrder.compute(data)

    companion object {
        fun build(baseEnvironment: BaseEnvironment, configure: EnvironmentBuilder.() -> Unit) =
            EnvironmentBuilder(baseEnvironment).apply(configure).build()
    }
}
