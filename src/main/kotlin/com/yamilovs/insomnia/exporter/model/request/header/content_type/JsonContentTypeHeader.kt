package com.yamilovs.insomnia.exporter.model.request.header.content_type

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import com.yamilovs.insomnia.exporter.model.request.mime_type.Application
import java.nio.charset.Charset

data class JsonContentTypeHeader(
    @get:JsonProperty("disabled")
    override val disabled: Boolean = false,

    @JsonIgnore
    private val charset: Charset = Charsets.UTF_8
) : ContentTypeHeaderBase() {

    @get:JsonProperty("value")
    override val value = "${Application.JSON.type}; charset=${charset.name().lowercase()}"

    @get:JsonProperty("description")
    override val description = "JavaScript Object Notation. RFC 4627"
}