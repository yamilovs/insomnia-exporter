package com.yamilovs.insomnia.exporter.model

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import com.yamilovs.insomnia.exporter.dsl.ApiSpecBuilder

data class ApiSpec(
    @JsonIgnore
    val workspace: Workspace,

    @get:JsonProperty("fileName")
    val fileName: String,
) : ResourceBase(Type.API_SPEC, workspace.id) {

    @get:JsonProperty("contents")
    val contents: String = ""

    @get:JsonProperty("contentType")
    val contentType: String = "yaml"

    companion object {
        fun build(workspace: Workspace, configure: ApiSpecBuilder.() -> Unit) =
            ApiSpecBuilder(workspace).apply(configure).build()
    }
}
