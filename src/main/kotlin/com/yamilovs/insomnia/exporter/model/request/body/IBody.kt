package com.yamilovs.insomnia.exporter.model.request.body

interface IBody {
    val mimeType: String
}