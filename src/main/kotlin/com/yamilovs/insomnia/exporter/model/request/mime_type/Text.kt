package com.yamilovs.insomnia.exporter.model.request.mime_type

enum class Text(override val subtype: String) : IMimeType {
    PLAIN("plain"),
    YAML("yaml");

    override val type: String = "text/$subtype"
}