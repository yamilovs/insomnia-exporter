package com.yamilovs.insomnia.exporter.model.request.authentication

import com.fasterxml.jackson.annotation.JsonIgnore

object EmptyAuthentication : IAuthentication {
    @get:JsonIgnore
    override val type = "Do not serialize me"
}