package com.yamilovs.insomnia.exporter.model

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import com.yamilovs.insomnia.exporter.dsl.RequestBuilder
import com.yamilovs.insomnia.exporter.model.request.FollowRedirect
import com.yamilovs.insomnia.exporter.model.request.Method
import com.yamilovs.insomnia.exporter.model.request.authentication.EmptyAuthentication
import com.yamilovs.insomnia.exporter.model.request.authentication.IAuthentication
import com.yamilovs.insomnia.exporter.model.request.body.EmptyBody
import com.yamilovs.insomnia.exporter.model.request.body.IBody
import com.yamilovs.insomnia.exporter.model.request.header.IHeader
import com.yamilovs.insomnia.exporter.model.request.parameter.IParameter


data class Request(
    @JsonIgnore
    private val parent: IRequestParent,

    @get:JsonProperty("url")
    val url: String,

    @get:JsonProperty("name")
    val name: String,

    @get:JsonProperty("description")
    val description: String = "",

    @get:JsonProperty("method")
    val method: Method = Method.GET,

    @get:JsonProperty("body")
    val body: IBody = EmptyBody,

    @get:JsonProperty("parameters")
    val parameters: List<IParameter> = listOf(),

    @get:JsonProperty("headers")
    val headers: List<IHeader> = listOf(),

    @get:JsonProperty("authentication")
    val authentication: IAuthentication = EmptyAuthentication,

    @get:JsonProperty("metaSortKey")
    val metaSortKey: Long = 0,

    @get:JsonProperty("isPrivate")
    val isPrivate: Boolean = false,

    @get:JsonProperty("settingStoreCookies")
    val settingStoreCookies: Boolean = true,

    @get:JsonProperty("settingSendCookies")
    val settingSendCookies: Boolean = true,

    @get:JsonProperty("settingDisableRenderRequestBody")
    val settingDisableRenderRequestBody: Boolean = false,

    @get:JsonProperty("settingEncodeUrl")
    val settingEncodeUrl: Boolean = true,

    @get:JsonProperty("settingRebuildPath")
    val settingRebuildPath: Boolean = true,

    @get:JsonProperty("settingFollowRedirects")
    val settingFollowRedirects: FollowRedirect = FollowRedirect.GLOBAL,
) : ResourceBase(Type.REQUEST, parent.id) {

    companion object {
        fun build(parent: IRequestParent, configure: RequestBuilder.() -> Unit) =
            RequestBuilder(parent).apply(configure).build()
    }
}
