package com.yamilovs.insomnia.exporter.model

enum class Scope {
    COLLECTION;

    override fun toString() = name.lowercase()
}