package com.yamilovs.insomnia.exporter.model.request.header

interface IHeader {
    val id: String

    val name: String

    val value: String

    val description: String

    val disabled: Boolean
}