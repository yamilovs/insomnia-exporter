package com.yamilovs.insomnia.exporter.model.request.header.content_type

import com.fasterxml.jackson.annotation.JsonProperty
import com.yamilovs.insomnia.exporter.model.request.header.HeaderBase

abstract class ContentTypeHeaderBase : HeaderBase() {
    @get:JsonProperty("name")
    override val name = "Content-Type"
}