package com.yamilovs.insomnia.exporter.model.request

enum class FollowRedirect {
    GLOBAL,
    ON,
    OFF;

    override fun toString() = name.lowercase()
}