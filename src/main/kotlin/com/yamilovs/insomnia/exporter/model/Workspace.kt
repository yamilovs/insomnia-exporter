package com.yamilovs.insomnia.exporter.model

import com.fasterxml.jackson.annotation.JsonProperty
import com.yamilovs.insomnia.exporter.dsl.WorkspaceBuilder

data class Workspace(
    @get:JsonProperty("name")
    val name: String = DEFAULT_NAME,

    @get:JsonProperty("description")
    val description: String = "",
) : ResourceBase(Type.WORKSPACE, null), IRequestParent, IRequestGroupParent {

    @get:JsonProperty("scope")
    val scope: Scope = Scope.COLLECTION

    companion object {
        const val DEFAULT_NAME = "My Collection"

        fun build(configure: WorkspaceBuilder.() -> Unit) = WorkspaceBuilder().apply(configure).build()
    }
}
