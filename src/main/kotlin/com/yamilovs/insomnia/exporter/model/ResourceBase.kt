package com.yamilovs.insomnia.exporter.model

import com.fasterxml.jackson.annotation.JsonProperty
import com.yamilovs.insomnia.exporter.utils.Hash
import java.time.Instant

abstract class ResourceBase(
    @get:JsonProperty("_type")
    override val type: Type,

    @get:JsonProperty("parentId")
    override val parentId: String?
) : IResource {

    private val now: Long = Instant.now().toEpochMilli()

    @get:JsonProperty("_id")
    override val id: String by lazy {
        type.prefix + "_" + Hash.generateRandomSha1()
    }

    @get:JsonProperty("modified")
    override val modified = now

    @get:JsonProperty("created")
    override val created = now
}
