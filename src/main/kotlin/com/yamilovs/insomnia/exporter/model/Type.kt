package com.yamilovs.insomnia.exporter.model

enum class Type(val prefix: String) {
    EXPORT(""),
    WORKSPACE("wrk"),
    API_SPEC("spc"),
    ENVIRONMENT("env"),
    REQUEST_GROUP("fld"),
    REQUEST("req"),
    PAIR("pair");

    override fun toString() = name.lowercase()
}