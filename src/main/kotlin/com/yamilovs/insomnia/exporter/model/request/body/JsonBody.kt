package com.yamilovs.insomnia.exporter.model.request.body

import com.fasterxml.jackson.annotation.JsonProperty
import com.yamilovs.insomnia.exporter.model.request.mime_type.Application

data class JsonBody(
    @get:JsonProperty("text")
    val text: String,
) : MimeTypeBody(Application.JSON)