package com.yamilovs.insomnia.exporter.model

import com.fasterxml.jackson.annotation.JsonProperty
import com.yamilovs.insomnia.exporter.dsl.CollectionBuilder
import java.time.Instant

/**
 * Main insomnia Collection wrapper.
 * Contains e.g. Workspace, ApiSpec, BaseEnvironment, Environments, RequestGroups and Requests
 */
class Collection(
    workspace: Workspace,
    apiSpec: ApiSpec,
    baseEnvironment: BaseEnvironment,
    environments: List<Environment> = listOf(),
    requestGroups: List<RequestGroup> = listOf(),
    requests: List<Request> = listOf(),
) {
    @get:JsonProperty("_type")
    val type: Type = Type.EXPORT

    @get:JsonProperty("__export_format")
    val exportFormat: Int = EXPORT_FORMAT

    @get:JsonProperty("__export_date")
    val exportDate = Instant.now().toString()

    @get:JsonProperty("__export_source")
    val exportSource = EXPORT_SOURCE

    @get:JsonProperty("resources")
    val resources: List<IResource> = listOf(
        workspace,
        apiSpec,
        baseEnvironment,
        *environments.toTypedArray(),
        *requestGroups.toTypedArray(),
        *requests.toTypedArray(),
    )

    companion object {
        private const val EXPORT_SOURCE = "yamilovs.insomnia.exporter"
        private const val EXPORT_FORMAT = 4

        fun build(configure: CollectionBuilder.() -> Unit): Collection = CollectionBuilder().apply(configure).build()
    }
}