package com.yamilovs.insomnia.exporter.model.request.mime_type

enum class Application(
    override val subtype: String
) : IMimeType {
    JSON("json"),
    XML("xml"),
    EDN("edn"),
    OCTET_STREAM("octet-stream"),
    X_WWW_FORM_URLENCODED("x-www-form-urlencoded");

    override val type: String = "application/$subtype"
}