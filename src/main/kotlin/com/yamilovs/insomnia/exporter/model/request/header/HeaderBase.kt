package com.yamilovs.insomnia.exporter.model.request.header

import com.fasterxml.jackson.annotation.JsonProperty
import com.yamilovs.insomnia.exporter.model.Type
import com.yamilovs.insomnia.exporter.utils.Hash

abstract class HeaderBase : IHeader {
    @get:JsonProperty("id")
    override val id = Type.PAIR.prefix + "_" + Hash.generateRandomSha1()
}