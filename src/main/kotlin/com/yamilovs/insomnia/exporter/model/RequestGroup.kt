package com.yamilovs.insomnia.exporter.model

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import com.yamilovs.insomnia.exporter.dsl.CollectionBuilder
import com.yamilovs.insomnia.exporter.dsl.RequestGroupBuilder
import com.yamilovs.insomnia.exporter.utils.PropertyOrder

data class RequestGroup(
    @JsonIgnore
    private val parent: IRequestGroupParent,

    @get:JsonProperty("name")
    val name: String,

    @get:JsonProperty("description")
    val description: String = "",

    @get:JsonProperty("environment")
    val environment: Map<String, Any> = mapOf(),

    @get:JsonProperty("metaSortKey")
    val metaSortKey: Long = 0,
) : ResourceBase(Type.REQUEST_GROUP, parent.id), IRequestParent, IRequestGroupParent {

    @get:JsonProperty("environmentPropertyOrder")
    val environmentPropertyOrder = PropertyOrder.compute(environment)

    companion object {
        fun build(builder: CollectionBuilder, parent: IRequestGroupParent, configure: RequestGroupBuilder.() -> Unit) =
            RequestGroupBuilder(builder, parent).apply(configure).build()
    }
}
