package com.yamilovs.insomnia.exporter.model.request.authentication

interface IAuthentication {
    val type: String
}