package com.yamilovs.insomnia.exporter.model.request.body

import com.fasterxml.jackson.annotation.JsonProperty
import com.yamilovs.insomnia.exporter.model.request.mime_type.IMimeType

open class MimeTypeBody(mime: IMimeType) : IBody {
    @get:JsonProperty("mimeType")
    override val mimeType: String = mime.type
}