package com.yamilovs.insomnia.exporter.model

interface IRequestGroupParent {
    val id: String
}