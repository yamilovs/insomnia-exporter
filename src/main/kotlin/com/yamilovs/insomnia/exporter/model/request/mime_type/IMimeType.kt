package com.yamilovs.insomnia.exporter.model.request.mime_type

interface IMimeType {
    val type: String
    val subtype: String
}