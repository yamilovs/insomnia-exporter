package com.yamilovs.insomnia.exporter.model.request.parameter

import com.fasterxml.jackson.annotation.JsonProperty
import com.yamilovs.insomnia.exporter.model.Type
import com.yamilovs.insomnia.exporter.utils.Hash

abstract class ParameterBase : IParameter {
    @get:JsonProperty("id")
    override val id = Type.PAIR.prefix + "_" + Hash.generateRandomSha1()
}
