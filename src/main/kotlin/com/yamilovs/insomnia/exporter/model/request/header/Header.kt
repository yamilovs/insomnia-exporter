package com.yamilovs.insomnia.exporter.model.request.header

import com.fasterxml.jackson.annotation.JsonProperty

data class Header(
    @get:JsonProperty("name")
    override val name: String,

    @get:JsonProperty("value")
    override val value: String,

    @get:JsonProperty("description")
    override val description: String = "",

    @get:JsonProperty("disabled")
    override val disabled: Boolean = false,
) : HeaderBase()
