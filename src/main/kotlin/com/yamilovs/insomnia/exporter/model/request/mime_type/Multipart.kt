package com.yamilovs.insomnia.exporter.model.request.mime_type

enum class Multipart(override val subtype: String) : IMimeType {
    FORM_DATA("form-data");

    override val type: String = "multipart/$subtype"
}