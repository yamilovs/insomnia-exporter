package com.yamilovs.insomnia.exporter.model

interface IRequestParent {
    val id: String
}