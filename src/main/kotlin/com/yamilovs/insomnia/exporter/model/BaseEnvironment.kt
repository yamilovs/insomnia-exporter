package com.yamilovs.insomnia.exporter.model

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import com.yamilovs.insomnia.exporter.dsl.BaseEnvironmentBuilder
import com.yamilovs.insomnia.exporter.dsl.CollectionBuilder
import com.yamilovs.insomnia.exporter.utils.PropertyOrder

data class BaseEnvironment(
    @JsonIgnore
    val workspace: Workspace,

    @get:JsonProperty("data")
    val data: Map<String, Any> = mapOf(),
) : ResourceBase(Type.ENVIRONMENT, workspace.id) {

    @get:JsonProperty("name")
    val name: String = "Base Environment"

    @get:JsonProperty("color")
    val color: String? = null

    @get:JsonProperty("isPrivate")
    val isPrivate: Boolean = false

    @get:JsonProperty("metaSortKey")
    val metaSortKey: Long = 0

    @get:JsonProperty("dataPropertyOrder")
    val dataPropertyOrder = PropertyOrder.compute(data)

    companion object {
        fun build(builder: CollectionBuilder, workspace: Workspace, configure: BaseEnvironmentBuilder.() -> Unit) =
            BaseEnvironmentBuilder(builder, workspace).apply(configure).build()
    }
}
