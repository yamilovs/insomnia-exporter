package com.yamilovs.insomnia.exporter.model.request.body

import com.fasterxml.jackson.annotation.JsonIgnore

object EmptyBody : IBody {
    @get:JsonIgnore
    override val mimeType = "Do not serialize me"
}