package com.yamilovs.insomnia.exporter.model

/**
 * This interface represents Insomnia resource in collection export
 */
interface IResource {
    /**
     * Represent identification of Insomnia resource with special resource prefix
     */
    val id: String

    /**
     * Represent identification string of the resource parent.
     * Can be null for root element only
     */
    val parentId: String?

    /**
     * Resource type
     */
    val type: Type

    /**
     * Timestamp when resource was modified
     */
    val modified: Long

    /**
     * Timestamp when resource was created
     */
    val created: Long
}