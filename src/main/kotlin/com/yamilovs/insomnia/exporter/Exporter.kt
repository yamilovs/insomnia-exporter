package com.yamilovs.insomnia.exporter

import com.fasterxml.jackson.databind.ObjectMapper
import com.yamilovs.insomnia.exporter.dsl.CollectionBuilder
import com.yamilovs.insomnia.exporter.model.Collection
import java.io.File
import java.io.OutputStream
import java.io.Writer

class Exporter(private val jackson: ObjectMapper) {
    fun writeValue(file: File, collection: Collection) = jackson.writeValue(file, collection)

    fun writeValue(file: File, configure: CollectionBuilder.() -> Unit) =
        jackson.writeValue(file, Collection.build(configure))

    fun writeValue(outputStream: OutputStream, collection: Collection) = jackson.writeValue(outputStream, collection)

    fun writeValue(outputStream: OutputStream, configure: CollectionBuilder.() -> Unit) =
        jackson.writeValue(outputStream, Collection.build(configure))

    fun writeValue(writer: Writer, collection: Collection) = jackson.writeValue(writer, collection)

    fun writeValue(writer: Writer, configure: CollectionBuilder.() -> Unit) =
        jackson.writeValue(writer, Collection.build(configure))

    fun writeValueAsString(collection: Collection): String? = jackson.writeValueAsString(collection)

    fun writeValueAsString(configure: CollectionBuilder.() -> Unit) =
        writeValueAsString(Collection.build(configure))

    companion object {
        val default by lazy { Exporter(Jackson.default) }
    }
}
