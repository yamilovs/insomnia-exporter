package com.yamilovs.insomnia.exporter.utils

import java.security.MessageDigest
import java.time.Instant
import kotlin.random.Random

internal object Hash {
    fun generateRandomSha1() = sha1(Instant.now().epochSecond.toString() + Random.nextLong())

    fun sha1(str: String) = MessageDigest.getInstance("SHA-1").digest(
        str.toByteArray()
    ).joinToString("") { "%02x".format(it) }
}