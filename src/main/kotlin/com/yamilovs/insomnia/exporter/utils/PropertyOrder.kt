package com.yamilovs.insomnia.exporter.utils

internal object PropertyOrder {
    private const val ROOT_KEY = "&"
    private const val KEY_SEPARATOR = "~|"

    fun compute(properties: Map<String, Any>): Map<String, List<String>>? {
        return properties.takeIf { it.isNotEmpty() }?.let {
            computeLevel(level = it)
        }
    }

    @Suppress("UNCHECKED_CAST")
    private fun computeLevel(
        level: Map<String, Any>,
        rootKey: String = ROOT_KEY,
        result: MutableMap<String, List<String>> = mutableMapOf()
    ): Map<String, List<String>> {

        result[rootKey] = level.keys.toList()

        level.filterValues { it is Map<*, *> }.forEach { (k, v) ->
            computeLevel(
                level = v as Map<String, Any>,
                rootKey = rootKey + KEY_SEPARATOR + k,
                result = result,
            )
        }

        return result
    }
}