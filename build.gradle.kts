import org.jetbrains.dokka.gradle.DokkaTask

val isRelease = project.hasProperty("release")

repositories {
    mavenLocal()
    mavenCentral()
}

plugins {
    kotlin("jvm") version "1.6.10"
    id("org.jetbrains.dokka") version "1.6.10"
    jacoco
    `maven-publish`
    signing
}

java {
    withJavadocJar()
    withSourcesJar()
}

tasks.withType<Test>().configureEach {
    useJUnitPlatform()
}

tasks.withType<DokkaTask> {
    dokkaSourceSets {
        named("main") {
            moduleName.set("Insomnia Exporter")

            suppressInheritedMembers.set(true)

            includes.from(
                "doc/dokka/module.md",
                "doc/dokka/packages.md",
            )

            samples.from(
                "src/test/kotlin/com/yamilovs/insomnia/exporter/dsl/Examples.kt",
            )

            perPackageOption {
                matchingRegex.set(""".*\.model.+""")
                suppress.set(true)
            }
        }
    }
}

publishing {
    repositories {
        maven {
            val repository = uri("https://s01.oss.sonatype.org/service/local/staging/deploy/maven2/")
            val snapshotRepository = uri("https://s01.oss.sonatype.org/content/repositories/snapshots/")

            url = if (isRelease) repository else snapshotRepository

            authentication {
                credentials {
                    username = System.getenv("MAVEN_USERNAME")
                    password = System.getenv("MAVEN_PASSWORD")
                }
            }
        }
    }

    publications {
        create<MavenPublication>("maven") {
            pom {
                name.set("Insomnia exporter")
                description.set("This library will help you to generate Insomnia collection `json` files with simple Kotlin DSL through your code")
                url.set("https://exporter.insomnia.yamilovs.com/")

                licenses {
                    license {
                        name.set("The Apache License, Version 2.0")
                        url.set("https://www.apache.org/licenses/LICENSE-2.0.txt")
                    }
                }
                developers {
                    developer {
                        id.set("yamilovs")
                        name.set("Stepan Yamilov")
                        email.set("yamilovs@gmail.com")
                    }
                }
                scm {
                    developerConnection.set("scm:git:git@gitlab.com:yamilovs/insomnia-exporter.git")
                    tag.set("master")
                    url.set("https://gitlab.com/yamilovs/insomnia-exporter")
                }
            }

            if (!isRelease) {
                version += "-SNAPSHOT"
            }

            from(components["java"])
        }
    }
}

signing {
    setRequired({
        isRelease && gradle.taskGraph.hasTask("publish")
    })

    val secretKey = System.getenv("GPG_PRIVATE_KEY")
    val password = System.getenv("GPG_PASSWORD")

    useInMemoryPgpKeys(secretKey, password)

    sign(publishing.publications)
}

dependencies {
    val kotestVersion: String by project
    val jacksonVersion: String by project

    implementation("com.fasterxml.jackson.core:jackson-databind:$jacksonVersion")

    testImplementation("io.kotest:kotest-runner-junit5:$kotestVersion")
    testImplementation("io.kotest:kotest-assertions-core:$kotestVersion")
    testImplementation("io.kotest:kotest-property:$kotestVersion")
}
