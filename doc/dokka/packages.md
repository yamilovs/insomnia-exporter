# Package com.yamilovs.insomnia.exporter

This package contains `Exporter`, the main entry point for working with this library

# Package com.yamilovs.insomnia.exporter.dsl

This package contains DSL builders, with handy code examples

# Package com.yamilovs.insomnia.exporter.model

This package contains all the DTO models that describes Insomnia collection structure

