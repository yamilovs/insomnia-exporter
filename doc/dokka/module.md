# Module Insomnia Exporter

[Insomnia](https://insomnia.rest/products/insomnia) is a great tool for building and testing APIs.

This library will help you to generate Insomnia collection `json` files with simple DSL through your code.
Then you can import this collection into your [insomnia client](https://insomnia.rest/download).

I hope you can find it valuable for creating testing cases or other purposes.
It's very handy for keeping your collection up to date and share requests through your developer team.

Take a look on simple example.

## Create Insomnia configuration file

```kotlin
val collection = Exporter.default.writeValueAsString {
    workspace { name { "Simple demo" } }
    request {
        name { "Get Google response" }
        url { "https://google.com" }
    }
}
println(collection)
```

And it's all.

You can replace getting collection to string with direct file creation:

```kotlin
Exporter.default.writeValue(File("insomnia.json")) {
    //...
}
```

As a result you will get `insomnia.json` collection file that you can import into your insomnia client.

Take a look at `com.yamilovs.insomnia.exporter.dsl` package - it contains DSL builder with code samples about all the features.
