# Full DSL features

```kotlin
Exporter.default.writeValue(File("insomnia.json")) {
    Exporter.default.writeValue(File("insomnia.json")) {
        // define workspace resource
        workspace {
            name { "Workspace name" }
            description { "About workspace" }
        }

        // define apiSpec resource
        apiSpec {
            fileName { "About workspace" }
        }

        // define baseEnvironment resource
        baseEnvironment {
            env { "BASE_URL" to "https://yamilovs.com" }
            env { "USERNAME" to "yamilovs" }

            environment {
                name { "Local" }
                env { "PASSWORD" to "123" }
                color { "#00FF00" }
                private { true }
                metaSortKey { 10 }
            }

            environment {
                name { "Production" }
                env { "PASSWORD" to "***" }
                color { "#FF0000" }
            }
        }

        // add request group resource
        requestGroup {
            name { "Simple requests" }
            description { "This group contains only simple request" }
            env { "PASSWORD" to "secure password" } // rewrite current environment value only for this group
            metaSortKey { 1 }

            requestGroup { // groups can be nested
                name { "Nested group" }
                description { "Only for demo purposes" }
            }

            request {
                name { "Check google" }
                url { "https://google.com" }
            }
        }

        // add request resource
        request {
            name { "Example of production bug" }
            description {
                """This request reproduces demo bug on demo production environment. 
                        | You must check it every day!""".trimMargin()
            }
            url { "https://yamilovs.com/" }
            method { Method.POST }
            body { JsonBody("[]") }
            header { JsonContentTypeHeader() }
            parameter { Parameter("show_debug", "true") }
            metaSortKey { 1 }
        }
    }
}
```